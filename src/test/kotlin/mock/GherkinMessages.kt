package mock

class FakeIds { companion object {
    const val Category = "1"
    const val Message = "2"
    const val Channel = "3"
    const val Guild = "4"
}}