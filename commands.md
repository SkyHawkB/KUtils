# Commands

## Key
| Symbol     | Meaning                    |
| ---------- | -------------------------- |
| (Argument) | This argument is optional. |

## Data
| Commands | Arguments | Description                                                                           |
| -------- | --------- | ------------------------------------------------------------------------------------- |
| DataSave | <none>    | This command lets you modify a Data object's contents.                                |
| DataSee  | <none>    | This command demonstrates loading and injecting Data objects by viewing its contents. |

## ServicesDemo
| Commands     | Arguments | Description              |
| ------------ | --------- | ------------------------ |
| DependsOnAll | <none>    | I depend on all services |

## Misc
| Commands    | Arguments | Description             |
| ----------- | --------- | ----------------------- |
| SomeCommand | <none>    | No Description Provider |

## Utility
| Commands           | Arguments                | Description                                                                      |
| ------------------ | ------------------------ | -------------------------------------------------------------------------------- |
| Add                | Integer, Integer         | Add two numbers together                                                         |
| ConversationTest   | <none>                   | Test the implementation of the ConversationDSL                                   |
| DiscordJsStringArg | JsStringArg, JsStringArg | This command demonstrates how to get a discord.js like string argument in Kutils |
| DisplayEmbed       | <none>                   | Display an example embed.                                                        |
| DisplayMenu        | <none>                   | Display an example menu.                                                         |
| Echo               | Text                     | No Description Provider                                                          |
| GuildOwner         | <none>                   | Provide info about the guild you executed the command in                         |
| GuildSize          | <none>                   | Display how many members are in a guild                                          |
| Help               | (Command)                | Display a help menu.                                                             |
| OptionalAdd        | Integer, (Integer)       | Add two numbers together                                                         |
| OptionalInput      | (Text)                   | Optionally input some text                                                       |
| Version            | <none>                   | A command which will show the version.                                           |

